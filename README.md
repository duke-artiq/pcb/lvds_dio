# Summary
This PCB converts betwee LVDS Signals carried over RJ45 and single-ended 3.3V TTL using the [SN65LVDM176](https://www.ti.com/lit/ds/symlink/sn65lvdm176.pdf?HQS=dis-dk-null-digikeymode-dsf-pf-null-wwe&ts=1644333470552&ref_url=https%253A%252F%252Fwww.ti.com%252Fgeneral%252Fdocs%252Fsuppproductinfo.tsp%253FdistId%253D10%2526gotoUrl%253Dhttps%253A%252F%252Fwww.ti.com%252Flit%252Fgpn%252Fsn65lvdm176) LVDS Transciever.

Channels can be individually configured as read or write through a solder-jumper. 

This board is designed for use with the [Sinara DIO_RJ45](https://github.com/sinara-hw/DIO_RJ45) board.
A grounded-connector shielded RJ-45 cable is recommended to ensure proper grounding between the LVDS nodes.

This board is based on the designs from the [OregonIons RJ45LVDS_SPI PCB](https://github.com/OregonIons/RJ45LVDS_SPI)

This board is powered from a 3.3V to 16V supply.

# Libraries
Please include a path variable `DQC_LIBRARIES` referenced to the [DQC KiCAD Libraries](https://gitlab.com/duke-artiq/pcb/dqc-kicad-libraries)

# Assembly
Please refer to the Assembly section of the [wiki](https://gitlab.com/duke-artiq/pcb/lvds_dio/-/wikis/home) for the assembly and unit testing procedure.

# License
